import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';
const execa = require('execa');
const crypto = require("crypto");

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('test')
  async getRes(@Res() req) {
    const cmd = "ls -la "+req.query.arg;

    const {stdout} = await execa.command(cmd); // Noncompliant
    return stdout;
  }

  @Get('hash')
  async getHash(@Res() req) {
    const hash = crypto.createHash('sha1'); // Sensitive
    return hash;
  }
}
