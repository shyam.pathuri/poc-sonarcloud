import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    if(1) {
      return 'Hello World!';
    }
    return 'not reachable'
  }

  get(): string {


    for (;;) {  // Noncompliant; end condition omitted
      // ...
    }
    
    var j = 0;
    while (true) { // Noncompliant; constant end condition
      j++;
    }
    
    var k;
    var b = true;
    while (b) { // Noncompliant; constant end condition
      k++;
    }
    
    if(1) {
      return 'Hello World!';
    }
    return 'not reachable'
  }
}
